package cz.cvut.fel.ts1;

public class Math {

    public int factorial(int a){
        if (a == 1) {
            return 1;
        }
        return a * factorial(a-1);
    }
}
